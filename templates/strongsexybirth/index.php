<?php

defined( '_JEXEC' ) or die;

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$user            = JFactory::getUser();
$this->language  = $doc->language;
$this->direction = $doc->direction;

// Getting params from template
$params = $app->getTemplate( true )->params;

// Detecting Active Variables
$option   = $app->input->getCmd( 'option', '' );
$view     = $app->input->getCmd( 'view', '' );
$layout   = $app->input->getCmd( 'layout', '' );
$task     = $app->input->getCmd( 'task', '' );
$itemid   = $app->input->getCmd( 'Itemid', '' );
$sitename = $app->get( 'sitename' );

if ( $task == "edit" || $layout == "form" ) {
	$fullWidth = 1;
} else {
	$fullWidth = 0;
}

// Add JavaScript Frameworks
JHtml::_( 'bootstrap.framework' );
$doc->addScript( $this->baseurl . '/templates/' . $this->template . '/js/template.js' );

// Add Stylesheets
$doc->addStyleSheet( $this->baseurl . '/templates/' . $this->template . '/css/template.css' );

// Load optional RTL Bootstrap CSS
JHtml::_( 'bootstrap.loadCss', false, $this->direction );

// Adjusting content width
if ( $this->countModules( 'right' ) && $this->countModules( 'left' ) ) {
	$span = "span6";
} elseif ( $this->countModules( 'right' ) && ! $this->countModules( 'left' ) ) {
	$span = "span9";
} elseif ( ! $this->countModules( 'right' ) && $this->countModules( 'left' ) ) {
	$span = "span9";
} else {
	$span = "span12";
}

// Logo file or site title param
if ( $params->get( 'logoFile' ) ) {
	$logo = '<img src="' . JUri::root() . $params->get( 'logoFile' ) . '" alt="' . $sitename . '" />';
} elseif ( $params->get( 'sitetitle' ) ) {
	$logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars( $params->get( 'sitetitle' ) ) . '</span>';
} else {
	$logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<jdoc:include type="head"/>
	<?php if ( $params->get( 'googleFont' ) ) : ?>
		<link href='//fonts.googleapis.com/css?family=<?php echo $params->get( 'googleFontName' ); ?>' rel='stylesheet' type='text/css'/>
	<?php endif; ?>
	<?php if ( $params->get( 'templateColor' ) ) : ?>
		<style type="text/css">
			body.site, body.site.fluid {
				background-color: <?php echo $params->get('templateBackgroundColor'); ?>
			}

			a {
				color: <?php echo $params->get('templateColor'); ?>;
			}
		</style>
	<?php endif; ?>
	<!--[if lt IE 9]>
	<script src="<?php echo JUri::root(true); ?>/media/jui/js/html5.js"></script>
	<![endif]-->
</head>

<body class="site <?php echo $option
                             . ' view-' . $view
                             . ( $layout ? ' layout-' . $layout : ' no-layout' )
                             . ( $task ? ' task-' . $task : ' no-task' )
                             . ( $itemid ? ' itemid-' . $itemid : '' )
                             . ( $params->get( 'fluidContainer' ) ? ' fluid' : '' );
?>">
<!-- Body -->
<div class="body">
	<div class="container<?php echo( $params->get( 'fluidContainer' ) ? '-fluid' : '' ); ?>">
		<!-- Header -->
		<header class="header" role="banner">
            <a class="header-logo" href="<?php echo $this->baseurl; ?>/"></a>
			<div class="header-inner clearfix">
				<a class="brand" href="<?php echo $this->baseurl; ?>/">
					<?php echo $logo; ?>
					<?php if ( $params->get( 'sitedescription' ) ) : ?>
						<?php echo '<div class="site-description">' . htmlspecialchars( $params->get( 'sitedescription' ) ) . '</div>'; ?>
					<?php endif; ?>
				</a>

				<div class="header-search">
					<jdoc:include type="modules" name="header" style="none"/>
				</div>
			</div>
		</header>
		<?php if ( $this->countModules( 'navigation' ) ) : ?>
			<nav class="navigation" role="navigation">
				<jdoc:include type="modules" name="navigation" style="none"/>
			</nav>
		<?php endif; ?>
		<jdoc:include type="modules" name="banner" style="xhtml"/>
		<div class="row-fluid">
			<?php if ( $this->countModules( 'left' ) ) : ?>
				<!-- Begin Sidebar -->
				<div id="sidebar" class="span2">
					<div class="sidebar-nav">
						<jdoc:include type="modules" name="left" style="xhtml"/>
					</div>
				</div>
				<!-- End Sidebar -->
			<?php endif; ?>
			<main id="content" role="main" class="<?php echo $span; ?>">
				<!-- Begin Content -->
				<jdoc:include type="modules" name="content-top" style="xhtml"/>
				<jdoc:include type="message"/>
				<jdoc:include type="component"/>
				<jdoc:include type="modules" name="content-bottom" style="none"/>
				<!-- End Content -->
			</main>
			<?php if ( $this->countModules( 'right' ) ) : ?>
				<div id="aside" class="span3">
					<!-- Begin Right Sidebar -->
					<jdoc:include type="modules" name="right" style="well"/>
					<!-- End Right Sidebar -->
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<!-- Footer -->
<footer class="footer" role="contentinfo">
	<div class="container<?php echo( $params->get( 'fluidContainer' ) ? '-fluid' : '' ); ?>">
		<jdoc:include type="modules" name="footer" style="none"/>
	</div>
</footer>
<jdoc:include type="modules" name="debug" style="none"/>
</body>
</html>
