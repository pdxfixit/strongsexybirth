<?php

defined('_JEXEC') or die;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$user = JFactory::getUser();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');

if ($task == "edit" || $layout == "form") {
    $fullWidth = 1;
} else {
    $fullWidth = 0;
}

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');

// Add Stylesheets
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/template.css');

// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Logo file or site title param
if ($params->get('logoFile')) {
    $logo = '<img src="' . JUri::root() . $params->get('logoFile') . '" alt="' . $sitename . '" />';
} elseif ($params->get('sitetitle')) {
    $logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($params->get('sitetitle')) . '</span>';
} else {
    $logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>"
      lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title><?php echo $sitename . ' - ' . $this->title . ' ' . htmlspecialchars($this->error->getMessage(), ENT_QUOTES, 'UTF-8'); ?></title>
    <?php if ($params->get('googleFont')) : ?>
        <link href="//fonts.googleapis.com/css?family=<?php echo $params->get('googleFontName'); ?>" rel="stylesheet" type="text/css"/>
    <?php endif; ?>
    <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/template.css" type="text/css"/>
    <?php if ($app->get('debug_lang', '0') == '1' || $app->get('debug', '0') == '1') : ?>
        <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/media/cms/css/debug.css" type="text/css"/>
    <?php endif; ?>
    <link href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/favicon.ico" rel="shortcut icon"
          type="image/vnd.microsoft.icon"/>
    <?php if ($params->get('templateColor')) : ?>
        <style type="text/css">
            body.site, body.site.fluid {
                background-color: <?php echo $params->get('templateBackgroundColor'); ?>
            }

            a {
                color: <?php echo $params->get('templateColor'); ?>;
            }
        </style>
    <?php endif; ?>
    <!--[if lt IE 9]>
    <script src="<?php echo JUri::root(true); ?>/media/jui/js/html5.js"></script>
    <![endif]-->
</head>

<body class="site <?php echo $option
    . ' view-' . $view
    . ($layout ? ' layout-' . $layout : ' no-layout')
    . ($task ? ' task-' . $task : ' no-task')
    . ($itemid ? ' itemid-' . $itemid : '')
    . ($params->get('fluidContainer') ? ' fluid' : '');
?>">
<!-- Body -->
<div class="body">
    <div class="container<?php echo($params->get('fluidContainer') ? '-fluid' : ''); ?>">
        <!-- Header -->
        <header class="header" role="banner">
            <a class="header-logo" href="<?php echo $this->baseurl; ?>/"></a>
            <div class="header-inner clearfix">
                <a class="brand" href="<?php echo $this->baseurl; ?>/">
                    <?php echo $logo; ?>
                    <?php if ($params->get('sitedescription')) : ?>
                        <?php echo '<div class="site-description">' . htmlspecialchars($params->get('sitedescription')) . '</div>'; ?>
                    <?php endif; ?>
                </a>

                <div class="header-search">
                    <?php echo $doc->getBuffer('modules', 'header', array('style' => 'none')); ?>
                </div>
            </div>
        </header>
        <nav class="navigation">
            <?php echo $doc->getBuffer('modules', 'navigation', array('style' => 'none')); ?>
        </nav>
        <?php echo $doc->getBuffer('modules', 'banner', array('style' => 'xhtml')); ?>
        <div class="row-fluid">
            <main id="content" role="main" class="span12">
                <!-- Begin Content -->
                <h1 class="page-header"><?php echo JText::_('JERROR_LAYOUT_PAGE_NOT_FOUND'); ?></h1>

                <div class="well">
                    <div class="span6">
                        <p>
                            <strong><?php echo JText::_('JERROR_LAYOUT_ERROR_HAS_OCCURRED_WHILE_PROCESSING_YOUR_REQUEST'); ?></strong>
                        </p>

                        <p><?php echo JText::_('JERROR_LAYOUT_NOT_ABLE_TO_VISIT'); ?></p>
                        <ul>
                            <li><?php echo JText::_('JERROR_LAYOUT_AN_OUT_OF_DATE_BOOKMARK_FAVOURITE'); ?></li>
                            <li><?php echo JText::_('JERROR_LAYOUT_MIS_TYPED_ADDRESS'); ?></li>
                            <li><?php echo JText::_('JERROR_LAYOUT_SEARCH_ENGINE_OUT_OF_DATE_LISTING'); ?></li>
                            <li><?php echo JText::_('JERROR_LAYOUT_YOU_HAVE_NO_ACCESS_TO_THIS_PAGE'); ?></li>
                        </ul>
                    </div>
                    <div class="span6">
                        <?php if (JModuleHelper::getModule('search')) : ?>
                            <p><strong><?php echo JText::_('JERROR_LAYOUT_SEARCH'); ?></strong></p>
                            <p><?php echo JText::_('JERROR_LAYOUT_SEARCH_PAGE'); ?></p>
                            <?php echo $doc->getBuffer('module', 'search'); ?>
                        <?php endif; ?>
                        <p><?php echo JText::_('JERROR_LAYOUT_GO_TO_THE_HOME_PAGE'); ?></p>

                        <p>
                            <a href="<?php echo $this->baseurl; ?>/index.php" class="btn"><i
                                    class="icon-home"></i> <?php echo JText::_('JERROR_LAYOUT_HOME_PAGE'); ?>
                            </a></p>
                    </div>
                    <hr style="clear: both;" />
                    <p>Please help us by <a href="/contact">reporting the problem</a>. Please provide the current URL, and the error message below.</p>
                    <blockquote>
                        <span
                            class="label label-inverse"><?php echo $this->error->getCode(); ?></span> <?php echo htmlspecialchars($this->error->getMessage(), ENT_QUOTES, 'UTF-8'); ?>
                    </blockquote>
                </div>
                <!-- End Content -->
            </main>
        </div>
    </div>
</div>
<!-- Footer -->
<footer class="footer">
    <div class="container<?php echo($params->get('fluidContainer') ? '-fluid' : ''); ?>">
        <?php echo $doc->getBuffer('modules', 'footer', array('style' => 'none')); ?>
    </div>
</footer>
<?php echo $doc->getBuffer('modules', 'debug', array('style' => 'none')); ?>
</body>
</html>
